from django.contrib import admin
from django.core.urlresolvers import reverse
from cashbox.models import Payment, PaymentAction


class ReadonlyAdmin(object):
    def get_readonly_fields(self, request, obj=None):
        return [field.name for field in self.model._meta.fields]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class PaymentActionInline(ReadonlyAdmin, admin.StackedInline):
    model = PaymentAction


class PaymentAdmin(ReadonlyAdmin, admin.ModelAdmin):
    list_display = ('invoice_id', 'backend', 'total_price_amount', 'total_price_currency', 'last_update', 'status')
    list_filter = ('backend', 'status')
    inlines = [
        PaymentActionInline,
    ]


class PaymentActionAdmin(ReadonlyAdmin, admin.ModelAdmin):
    list_display = ('backend', 'payment_link', 'datetime', 'internal_exception')
    list_filter = ('internal_exception', 'backend')
    date_hierarchy = 'datetime'

    def payment_link(self, obj):
        if obj.payment:
            return '<a href="%s">%s</a>' % (
                reverse('admin:cashbox_payment_change', args=(obj.payment.id,)), obj.payment
            )
        return '---'

    payment_link.allow_tags = True


admin.site.register(Payment, PaymentAdmin)
admin.site.register(PaymentAction, PaymentActionAdmin)