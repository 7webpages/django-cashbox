def import_class(path_to_class):
    mod = __import__(path_to_class)
    components = path_to_class.split('.')
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod
