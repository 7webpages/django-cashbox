class BaseProduct(object):
    """
    Base interface for product-like objects
    """
    def get_title(self):
        """
        Returns:
            unicode: Product title
        """
        raise NotImplementedError("Redefine this in derived classes")

    def get_code(self):
        """
        Returns:
            unicode: Product code
        """
        raise NotImplementedError("Redefine this in derived classes")

    def get_price_amount(self):
        """
        Returns:
            float: Product price
        """
        raise NotImplementedError("Redefine this in derived classes")

    def get_price_currency(self):
        """
        Returns:
            str: Price currency
        """
        raise NotImplementedError("Redefine this in derived classes")
