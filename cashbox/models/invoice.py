class BaseInvoice(object):
    """
    Base interface for invoice-like objects
    """
    def get_id(self):
        """
        Returns:
            str: Unique id for the invoice
        """
        raise NotImplementedError("Redefine this in derived classes")

    def get_creation_datetime(self):
        """
        Returns:
            datetime: Date when invoice was created
        """
        raise NotImplementedError("Redefine this in derived classes")

    def get_expiration_datetime(self):
        """
        Returns:
            datetime: Date until this invoice is valid
        """
        raise NotImplementedError("Redefine this in derived classes")

    def get_products(self):
        """
        Returns:
            list of BaseProduct
        """
        raise NotImplementedError("Redefine this in derived classes")
