from django.db import models
from django.forms import model_to_dict
from json_field import JSONField
from cashbox.requests import NotificationRequest
from cashbox.responses import BaseNotificationResponse
from payment import Payment


__all__ = ['PaymentAction']


class PaymentActionManager(models.Manager):
    def log_action(self, request, response):
        """
        Writes information about success payment processing
        """
        assert isinstance(request, NotificationRequest)
        assert isinstance(response, BaseNotificationResponse)

        self.create(
            payment=response.payment,
            payment_state=model_to_dict(response.payment),
            backend=request.backend_code,
            request=request.to_dict(),
            response=response.to_dict()
        )

    def log_exception(self, request, traceback):
        """
        Writes information about failed payment processing
        """
        assert isinstance(request, NotificationRequest)
        assert isinstance(traceback, basestring)

        self.create(
            backend=request.backend_code,
            request=request.to_dict(),
            internal_exception=True,
            internal_exception_traceback=traceback
        )


class PaymentAction(models.Model):
    """
    Information about all actions with a payment object
    """
    payment = models.ForeignKey(Payment, related_name='actions', blank=True, null=True)
    payment_state = JSONField()
    backend = models.CharField(max_length=64)
    request = JSONField()
    response = JSONField()
    datetime = models.DateTimeField(auto_now_add=True)
    internal_exception = models.BooleanField(default=False)
    internal_exception_traceback = models.TextField()

    objects = PaymentActionManager()

    class Meta:
        app_label = 'cashbox'
