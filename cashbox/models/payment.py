from django.db import models
from django.utils.translation import ugettext_lazy as _
from json_field import JSONField


__all__ = ['Payment']


class Payment(models.Model):
    """
    Information about payment's state
    """
    STATUS_PENDING = 'pending'
    STATUS_AUTHORIZED = 'authorized'
    STATUS_COMPLETE = 'complete'
    STATUS_REFUND = 'refund'
    STATUS_REVERSED = 'reversed'
    STATUS_FAILED = 'failed'

    STATUS_CHOICES = (
        (STATUS_PENDING, _("Pending")),
        (STATUS_AUTHORIZED, _("Authorized")),
        (STATUS_COMPLETE, _("Complete")),
        (STATUS_REFUND, _("Refund")),
        (STATUS_REVERSED, _("Reversed")),
        (STATUS_FAILED, _("Failed"))
    )

    backend = models.CharField(max_length=64)
    status = models.CharField(choices=STATUS_CHOICES, max_length=32)
    invoice_id = models.CharField(unique=True, max_length=128)
    total_price_amount = models.FloatField()
    total_price_currency = models.CharField(max_length=3)
    last_update = models.DateTimeField(auto_now=True)
    metadata = JSONField()

    class Meta:
        app_label = 'cashbox'

    def __unicode__(self):
        return u"%s - %0.2f %s" % (self.invoice_id, self.total_price_amount, self.total_price_currency)
