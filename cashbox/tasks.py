import logging
from celery import Task
from cashbox.models import Payment


__all__ = ['RefundPaymentTask', 'FinishPaymentTask']

log = logging.getLogger('cashbox.CashboxService')


class RefundPaymentTask(Task):
    queue = 'cashbox'
    max_retries = 3
    default_retry_delay = 180  # 3 minutes

    def run(self, backend, payment_pk):
        try:
            payment = Payment.objects.get(pk=payment_pk)
            backend().refund(payment)
        except Exception as exc:
            log.exception("Can't refund payment")
            self.retry(exc=exc)


class FinishPaymentTask(Task):
    queue = 'cashbox'
    max_retries = 3
    default_retry_delay = 180  # 3 minutes

    def run(self, backend, payment_pk):
        try:
            payment = Payment.objects.get(pk=payment_pk)
            backend().finish(payment)
        except Exception as exc:
            log.exception("Can't complete payment")
            self.retry(exc=exc)
