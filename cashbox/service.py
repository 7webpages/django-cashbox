import logging
import traceback
from cashbox.backends.deposits.backend import DepositsPaymentBackend
from cashbox.backends.payu import PayUPaymentBackend
from cashbox.exceptions import UnsupportedBackend
from cashbox.models import Payment, PaymentAction
from cashbox.requests import NotificationRequest
from cashbox.tasks import RefundPaymentTask, FinishPaymentTask
from cashbox.signals import payment_authorized, payment_completed, payment_refunded


__all__ = ['CashboxService']

log = logging.getLogger('cashbox.CashboxService')


class CashboxService(object):
    """
    Allows to manage payments
    """
    enabled_backends = [PayUPaymentBackend, DepositsPaymentBackend]

    def process_notification(self, notification_request):
        """
        Processes requests which come from external payment systems

        Args:
            notification_request (requests.NotificationRequest): Wrapped http request

        Returns:
            responses.BaseNotificationResponse

        Raises:
            UnsupportedBackend: payment was received from backend, which isn't added to enabled_backends
        """
        assert isinstance(notification_request, NotificationRequest)

        backend = self._get_backend(notification_request.backend_code)

        try:
            notification_response = backend().process_notification(notification_request)
            payment = notification_response.payment
            payment.backend = backend.CODE

            # Get information about previous payment status
            try:
                prev_status = Payment.objects.get(pk=payment.pk).status
            except Payment.DoesNotExist:
                prev_status = None

            # Save new payment state
            payment.save()

            # Send signal about new payment state if it was updated
            if prev_status != payment.status:
                if payment.status == Payment.STATUS_AUTHORIZED:
                    payment_authorized.send(sender=self, payment=payment)
                elif payment.status == Payment.STATUS_COMPLETE:
                    payment_completed.send(sender=self, payment=payment)
                elif payment.status == Payment.STATUS_REFUND:
                    payment_refunded.send(sender=self, payment=payment)

            PaymentAction.objects.log_action(notification_request, notification_response)

            return notification_response

        except Exception:
            PaymentAction.objects.log_exception(notification_request, traceback.format_exc())
            raise

    def refund(self, payment):
        """
        Requests refunding in external system for the given payment

        Args:
            payment (models.Payment): our internal record about payment

        Returns:
            AsyncResult: RefundPaymentTask AsyncResult

        Raises:
            UnsupportedBackend: payment was received from backend, which isn't added to enabled_backends
        """
        assert isinstance(payment, Payment)
        assert payment.backend is not None

        backend = self._get_backend(payment.backend)

        return RefundPaymentTask().delay(backend, payment.pk)

    def finish(self, payment):
        """
        Confirms product delivery for given payment (completes payment operation)

        Args:
            payment (models.Payment): our internal record about payment

        Returns:
            AsyncResult: FinishPaymentTask AsyncResult

        Raises:
            UnsupportedBackend: payment was received from backend, which isn't added to enabled_backends
        """
        assert isinstance(payment, Payment)
        assert payment.backend is not None

        backend = self._get_backend(payment.backend)

        return FinishPaymentTask().delay(backend, payment.pk)

    def get_backends(self):
        """
        Returns all connected payment backends

        Returns:
            list of BasePaymentBackend objects
        """
        return self.enabled_backends

    def _get_backend(self, backend_code):
        for backend in self.enabled_backends:
            if backend.CODE == backend_code:
                return backend

        raise UnsupportedBackend()