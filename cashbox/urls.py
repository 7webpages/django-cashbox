from django.conf.urls import patterns, url

urlpatterns = patterns(
    'cashbox.views',

    url(r'^(?P<backend_code>[\w_-]+)/(?P<backend_method>[\w_-]+)/$', 'cashbox_router', name='cashbox_router'),
)