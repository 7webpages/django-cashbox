from django.dispatch import Signal


payment_authorized = Signal(providing_args=['payment'])  # Payment was approved by user (we can charge money)
payment_completed = Signal(providing_args=['payment'])  # Order was delivered to the user
payment_refunded = Signal(providing_args=['payment'])  # Payment was successfully refunded
