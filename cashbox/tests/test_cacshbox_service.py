from celery.result import AsyncResult
from django.forms import model_to_dict
from django.test import TestCase
from django.test.utils import override_settings
from mock import Mock, patch
from cashbox.backends.base import BasePaymentBackend
from cashbox.exceptions import UnsupportedBackend
from cashbox.models import Payment, PaymentAction
from cashbox.requests import NotificationRequest
from cashbox.responses import NotificationResponse
from cashbox.service import CashboxService
from cashbox.signals import payment_authorized, payment_completed, payment_refunded


__all__ = ['CashboxServiceTest']


class FirstTestPaymentBackend(BasePaymentBackend):
    CODE = 'first'


class SecondTestPaymentBackend(BasePaymentBackend):
    CODE = 'second'


class CashboxServiceTest(TestCase):
    def setUp(self):
        payment_authorized.receivers = []
        payment_completed.receivers = []
        payment_refunded.receivers = []

    def test_process_notification(self):
        """
        1. Define CashboxService and attach one backend
        2. Define notification request
        3. Define notification response
        4. Mock backend process_notification method with success response
        5. Call method and check results
        6. Mock backend process_notification method with exception
        7. Call method and check results
        """
        # Step 1
        service = CashboxService()
        service.enabled_backends = [FirstTestPaymentBackend]

        # Step 2
        notification_request = NotificationRequest(
            backend_code=FirstTestPaymentBackend.CODE,
            backend_method='ipn',
            request_method='POST',
            get_args={},
            post_args={
                'status': 'OK'
            },
            meta={
                'user_ip': '127.0.0.1'
            }
        )

        # Step 3
        notification_response = NotificationResponse(
            payment=Payment(
                status=Payment.STATUS_AUTHORIZED,
                invoice_id='123',
                total_price_amount=100.50,
                total_price_currency='UAH',
                metadata={
                    'internal-backend-id': '321'
                }
            ),
            response='ACCEPTED'
        )

        # Step 4
        FirstTestPaymentBackend.process_notification = Mock(return_value=notification_response)

        # Step 5
        service_response = service.process_notification(notification_request)

        self.assertEqual(notification_response, service_response)
        FirstTestPaymentBackend.process_notification.assert_called_with(notification_request)

        self.assertEqual(1, Payment.objects.count())
        self.assertEqual(1, PaymentAction.objects.count())
        payment, action = Payment.objects.get(), PaymentAction.objects.get()

        self.assertEqual(FirstTestPaymentBackend.CODE, payment.backend)
        self.assertEqual(payment, action.payment)
        self.assertEqual(model_to_dict(action.payment), action.payment_state)
        self.assertEqual(FirstTestPaymentBackend.CODE, action.backend)
        self.assertEqual(notification_request.to_dict(), action.request)
        self.assertEqual(notification_response.to_dict(), action.response)
        self.assertIsNotNone(action.datetime)
        self.assertFalse(action.internal_exception)

        PaymentAction.objects.all().delete()

        # Step 6
        FirstTestPaymentBackend.process_notification = Mock(side_effect=[ValueError])

        # Step 7
        self.assertRaises(ValueError, service.process_notification, notification_request)
        self.assertEqual(1, PaymentAction.objects.count())

        action = PaymentAction.objects.get()
        self.assertIsNone(action.payment)
        self.assertIsNone(action.payment_state)
        self.assertEqual(FirstTestPaymentBackend.CODE, action.backend)
        self.assertEqual(notification_request.to_dict(), action.request)
        self.assertIsNone(action.response)
        self.assertIsNotNone(action.datetime)
        self.assertTrue(action.internal_exception)
        self.assertIsNotNone(action.internal_exception_traceback)

    @patch('cashbox.service.payment_authorized')
    @patch('cashbox.service.payment_completed')
    @patch('cashbox.service.payment_refunded')
    def test_signals_generation(self, payment_refunded_mock, payment_completed_mock, payment_authorized_mock):
        """
        1. Define CashboxService and attach one backend
        2. Define fake notification request
        3. Define notification response with new authorized payment
        4. Define notification response with same but confirmed payment
        5. Define notification response with same but refunded state
        """
        payment_refunded_mock.send = Mock()
        payment_completed_mock.send = Mock()
        payment_authorized_mock.send = Mock()

        # Step 1
        service = CashboxService()
        service.enabled_backends = [FirstTestPaymentBackend]

        # Step 2
        notification_request = NotificationRequest(
            backend_code=FirstTestPaymentBackend.CODE,
            backend_method='ipn',
            request_method='POST',
            get_args={},
            post_args={
                'status': 'OK'
            },
            meta={
                'user_ip': '127.0.0.1'
            }
        )

        # Step 3
        payment = Payment(
            status=Payment.STATUS_AUTHORIZED,
            invoice_id='123',
            total_price_amount=100.50,
            total_price_currency='UAH'
        )

        notification_response = NotificationResponse(payment=payment, response='ACCEPTED')
        FirstTestPaymentBackend.process_notification = Mock(return_value=notification_response)
        notification_response = service.process_notification(notification_request)
        payment_authorized_mock.send.assert_called_once_with(payment=notification_response.payment, sender=service)

        # Step 4
        payment.status = Payment.STATUS_COMPLETE
        notification_response = NotificationResponse(payment=payment, response='ACCEPTED')
        FirstTestPaymentBackend.process_notification = Mock(return_value=notification_response)
        service.process_notification(notification_request)
        payment_completed_mock.send.assert_called_once_with(payment=notification_response.payment, sender=service)

        # Step 5
        payment.status = Payment.STATUS_REFUND
        notification_response = NotificationResponse(payment=payment, response='ACCEPTED')
        FirstTestPaymentBackend.process_notification = Mock(return_value=notification_response)
        service.process_notification(notification_request)
        payment_refunded_mock.send.assert_called_once_with(payment=notification_response.payment, sender=service)

    @patch('cashbox.service.RefundPaymentTask')
    def test_refund(self, refund_payment_task_mock):
        """
        1. Define CashboxService and attach one backend
        2. Define payment object from attached backend
        3. Call method and check RefundTask arguments
        4. Define payment object from unattached backend and call method
        """
        # Step 1
        service = CashboxService()
        service.enabled_backends = [FirstTestPaymentBackend]

        # Step 2
        payment = Payment(backend=FirstTestPaymentBackend.CODE, pk=1)

        # Step 3
        refund_payment_task = Mock(**{'delay.return_value': AsyncResult('1')})
        refund_payment_task_mock.return_value = refund_payment_task

        response = service.refund(payment)

        self.assertIsInstance(response, AsyncResult)
        refund_payment_task.delay.assert_called_with(FirstTestPaymentBackend, payment.pk)

        # Step 4
        payment.backend = SecondTestPaymentBackend.CODE
        self.assertRaises(UnsupportedBackend, service.refund, payment)

    @patch('cashbox.service.FinishPaymentTask')
    def test_finish(self, finish_payment_task_mock):
        """
        1. Define CashboxService and attach one backend
        2. Define payment object from attached backend
        3. Call method and check which backend and which method were called
        4. Define payment object from unattached backend and call method
        """
        # Step 1
        service = CashboxService()
        FirstTestPaymentBackend.finish = Mock()
        service.enabled_backends = [FirstTestPaymentBackend]

        # Step 2
        payment = Payment(backend=FirstTestPaymentBackend.CODE, pk=1)

        # Step 3
        finish_payment_task = Mock(**{'delay.return_value': AsyncResult('1')})
        finish_payment_task_mock.return_value = finish_payment_task

        response = service.finish(payment)

        self.assertIsInstance(response, AsyncResult)
        finish_payment_task.delay.assert_called_with(FirstTestPaymentBackend, payment.pk)

        # Step 4
        payment.backend = SecondTestPaymentBackend.CODE
        self.assertRaises(UnsupportedBackend, service.finish, payment)

    def test_get_backends(self):
        """
        1. Define CashboxService and attach two backends
        2. Call method and check results
        """
        # Step 1
        service = CashboxService()
        backends = [FirstTestPaymentBackend, SecondTestPaymentBackend]
        service.enabled_backends = backends

        # Step 2
        self.assertEqual(backends, service.get_backends())
