# coding=utf-8
from django.utils.datastructures import SortedDict
from mock import Mock
from django.http import HttpResponse
from django.test import TestCase, RequestFactory
from cashbox.models import Payment
from cashbox.requests import NotificationRequest
from cashbox.responses import NotificationResponse
from cashbox.views import CashboxRouterView


__all__ = ['CashboxRouterViewTest']


class CashboxRouterViewTest(TestCase):
    def test_post_query(self):
        """
        1. Mock CashboxService process_notification method with successful response
        2. Generate fake post request
        3. Process request and check results
        4. Generate fake get request
        5. Process request and check results
        """
        # Step 1
        router_view = CashboxRouterView()
        router_view.cashbox_service.process_notification = Mock(
            return_value=NotificationResponse(payment=Payment(), response='OK')
        )

        # Step 2
        factory = RequestFactory()
        post_request = factory.generic(
            method='POST',
            path='/cashbox/payu/ipn/',
            data=u'c=2&b=1&a=4&a=3&d=Юникод'
        )

        # Step 3
        response = router_view.dispatch(post_request, backend_code='payu', backend_method='ipn')
        self.assertEqual(200, response.status_code)
        self.assertIsInstance(response, HttpResponse)

        call_args, call_kwargs = router_view.cashbox_service.process_notification.call_args
        self.assertEqual(1, len(call_args))
        notification_request = call_args[0]

        self.assertIsInstance(notification_request, NotificationRequest)
        self.assertEqual('payu', notification_request.backend_code)
        self.assertEqual('ipn', notification_request.backend_method)
        self.assertEqual('POST', notification_request.request_method)
        self.assertEqual(SortedDict(), notification_request.get_args)
        self.assertEqual(SortedDict([
            ('c', '2'),
            ('b', '1'),
            ('a', ['4', '3']),
            ('d', u"Юникод")
        ]), notification_request.post_args)

        # Step 4
        get_request = factory.generic(
            method='GET',
            path=u'/cashbox/payu/ipn/?c=2&b=1&a=4&a=3&d=Юникод'
        )

        # Step 5
        response = router_view.dispatch(get_request, backend_code='payu', backend_method='ipn')
        self.assertEqual(200, response.status_code)
        self.assertIsInstance(response, HttpResponse)

        call_args, call_kwargs = router_view.cashbox_service.process_notification.call_args
        self.assertEqual(1, len(call_args))
        notification_request = call_args[0]

        self.assertIsInstance(notification_request, NotificationRequest)
        self.assertEqual(SortedDict([
            ('c', '2'),
            ('b', '1'),
            ('a', ['4', '3']),
            ('d', u"Юникод")
        ]), notification_request.get_args)
