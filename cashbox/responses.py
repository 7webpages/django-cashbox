from copy import deepcopy
from cashbox.models import Payment


class BaseNotificationResponse(object):
    """
    Base class for all responses from backends
    """
    def __init__(self, payment):
        assert isinstance(payment, Payment)

        self.payment = payment

    def to_dict(self):
        data = deepcopy(self.__dict__)
        if 'payment' in data:
            del data['payment']
        return data


class NotificationResponseRedirect(BaseNotificationResponse):
    """
    CashboxRouter should redirect client to the certain url
    """
    def __init__(self, payment, url):
        assert isinstance(url, basestring)

        self.url = url
        super(NotificationResponseRedirect, self).__init__(payment)


class NotificationResponse(BaseNotificationResponse):
    """
    CashboxRouter should give `response` to the client
    """
    def __init__(self, payment, response):
        assert isinstance(response, basestring)

        self.response = response
        super(NotificationResponse, self).__init__(payment)
