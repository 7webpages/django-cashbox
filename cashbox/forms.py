class BasePaymentForm(object):
    """
    Attributes:
        action (str)
        method (str)

    Usage example::
        <form method="{{ form.method }}" action="{{ form.action }}">
            {{ form.render|safe }}
            <input type="submit" value="Pay" />
        </form>

    Note! It isn't django form.
    """
    action = 'action-url'
    method = 'POST or GET'

    def __init__(self, invoice, return_url, metadata=None):
        self.invoice = invoice
        self.return_url = return_url
        self.metadata = metadata or {}

    def render(self):
        """
        Returns:
            unicode: hidden html input fields with payment details
        """
        raise NotImplementedError()
