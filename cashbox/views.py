from urlparse import parse_qsl
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.utils.datastructures import SortedDict
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from cashbox.exceptions import UnsupportedBackend
from cashbox.requests import NotificationRequest
from cashbox.responses import NotificationResponse, NotificationResponseRedirect
from cashbox.service import CashboxService


__all__ = ['cashbox_router']


class CashboxRouterView(View):
    cashbox_service = CashboxService()

    def dispatch(self, request, *args, **kwargs):
        notification_request = NotificationRequest(
            backend_code=kwargs['backend_code'],
            backend_method=kwargs['backend_method'],
            request_method=request.method,
            get_args=self._parse_ordered_get_args(request),
            post_args=self._parse_ordered_post_args(request),
            meta={
                'user_ip': request.META.get('REMOTE_ADDR'),
                'user_agent': request.META.get('HTTP_USER_AGENT'),
                'content_type': request.META.get('CONTENT_TYPE')
            }
        )

        try:
            notification_response = self.cashbox_service.process_notification(notification_request)

            if isinstance(notification_response, NotificationResponse):
                return HttpResponse(notification_response.response)

            if isinstance(notification_response, NotificationResponseRedirect):
                return HttpResponseRedirect(notification_response.url)

        except UnsupportedBackend:
            return HttpResponseBadRequest('Unsupported backend')

    def _parse_ordered_get_args(self, request):
        return SortedDict(self._parse_ordered_qs(request.META['QUERY_STRING']))

    def _parse_ordered_post_args(self, request):
        if request.method == 'POST':
            return SortedDict(self._parse_ordered_qs(request.body))

        return SortedDict()

    def _parse_ordered_qs(self, query_string, keep_blank_values=True, strict_parsing=False):
        """
        Django doesn't guarantee order of data parsed from query_string. We have to do this by ourselves,
        because, for example, payu logic is based on correct order of post data
        """
        data = SortedDict()

        for name, value in parse_qsl(query_string, keep_blank_values, strict_parsing):
            value = value.decode('utf-8')
            if name in data:
                prev_value = data[name]
                if isinstance(prev_value, list):
                    data[name] = prev_value + [value]
                else:
                    data[name] = [data[name], value]
            else:
                data[name] = value

        return data


cashbox_router = csrf_exempt(CashboxRouterView.as_view())
