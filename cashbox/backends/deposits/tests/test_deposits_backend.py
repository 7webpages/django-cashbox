# coding=utf-8
from datetime import datetime
import os
from django.conf import settings
from django.test import TestCase
from django.test.utils import override_settings
from django.utils.datastructures import SortedDict
from mock import patch, Mock
from cashbox.backends.deposits.backend import DepositsPaymentBackend
from cashbox.exceptions import InvalidResponse
from cashbox.models import BaseInvoice, BaseProduct, Payment
from cashbox.requests import NotificationRequest
from cashbox.responses import NotificationResponse


__all__ = ['DepositsBackendTest']


@override_settings(
    DEPOSITS_REGISTRATION_URL='http://example.com/invoices/',
    DEPOSITS_NOTIFICATION_URL='http://example.com/notification/',
    DEPOSITS_REFUND_URL='http://example.com/invoices/refund/',
    DEPOSITS_PROVIDER='deposits.example.com',
    DEPOSITS_SECRET='test'
)
class DepositsBackendTest(TestCase):
    HTML_DIRECTORY = os.path.join(os.path.dirname(__file__), 'html_examples')
    backend = DepositsPaymentBackend()
    maxDiff = None

    @patch('cashbox.backends.deposits.backend.datetime')
    def test_process_ipn_notification(self, datetime_mock):
        """
        1. Define IPN notification request
        2. Call process_notification method
        3. Check response
        """
        datetime_mock.now = Mock(return_value=datetime(2012, 4, 26, 12, 34, 34))

        # Step 1
        notification_request = NotificationRequest(
            backend_code='payu',
            backend_method='ipn',
            request_method='POST',
            get_args={},
            post_args=SortedDict([
                ('internal_id', '1'),
                ('invoice_id', 'test_invoice'),
                ('amount', '5420.20'),
                ('currency', 'UAH'),
                ('status', 'completed'),
                ('signature', '3a83b43804a63d667593e937a572f28b')
            ])
        )

        # Step 2
        notification_response = self.backend.process_notification(notification_request)

        # Step 3
        response, payment = notification_response.response, notification_response.payment

        self.assertIsInstance(notification_response, NotificationResponse)
        self.assertEqual('<response>673869511f1256592b165bb4ca3bde80|2012-04-26T12:34:34</response>', response)

        self.assertIsInstance(payment, Payment)
        self.assertEqual(Payment.STATUS_AUTHORIZED, payment.status)
        self.assertEqual('test_invoice', payment.invoice_id)
        self.assertEqual(5420.20, payment.total_price_amount)
        self.assertEqual('UAH', payment.total_price_currency)
        self.assertEqual('1', payment.metadata.get('internal_id'))

    @patch('cashbox.backends.deposits.backend.requests')
    def test_refund(self, requests_mock):
        """
        1. Define fake payment object
        2. Mock request.post with successful response
        3. Mock request.post with failed response
        """
        # Step 1
        payment = Payment(
            invoice_id='test-invoice'
        )

        # Step 2
        requests_mock.post = Mock(return_value=Mock(status_code=200, text='REFUNDED'))
        self.backend.refund(payment)

        requests_mock.post.assert_called_once_with(settings.DEPOSITS_REFUND_URL, data={
            'provider': 'deposits.example.com',
            'invoice_id': 'test-invoice',
            'signature': '169d5f698b52c5e5ea80dae68802cc0e'
        })

        # Step 3
        requests_mock.post = Mock(return_value=Mock(status_code=201, text='ERROR'))
        self.assertRaises(InvalidResponse, self.backend.refund, payment)

    def test_get_form(self):
        """
        1. Create simple invoice with one product
        2. Call method and check results
        """
        # Step 1
        class Product(BaseProduct):
            def __init__(self, title):
                self.title = title

            def get_title(self):
                return self.title

            def get_price_amount(self):
                return 2710.00

            def get_price_currency(self):
                return 'UAH'

        class Invoice(BaseInvoice):
            def get_id(self):
                return 'new-invoice'

            def get_creation_datetime(self):
                return datetime(2013, 12, 21, 10)

            def get_expiration_datetime(self):
                return datetime(2013, 12, 21, 12)

            def get_products(self):
                return [
                    Product('Product #1'),
                    Product('Product #2'),
                ]

        # Step 2
        form = self.backend.get_form(
            Invoice(), 'http://example.com/', metadata=dict(deposit_account='deposits@example.com')
        )
        sample_html = open(os.path.join(self.HTML_DIRECTORY, 'payment_form.html')).read().decode('utf-8')

        self.assertEqual('POST', form.method)
        self.assertHTMLEqual(sample_html, form.render())
