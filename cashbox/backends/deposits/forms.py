from django import forms
from django.conf import settings
from cashbox.backends.deposits.utils import calculate_signature
from cashbox.forms import BasePaymentForm


__all__ = ['DepositsPaymentForm']


class DepositsPaymentForm(BasePaymentForm, forms.Form):
    method = 'POST'
    action = settings.DEPOSITS_REGISTRATION_URL

    provider = forms.CharField(widget=forms.HiddenInput)
    invoice_id = forms.CharField(widget=forms.HiddenInput)
    payment_amount = forms.FloatField(widget=forms.HiddenInput)
    payment_currency = forms.CharField(widget=forms.HiddenInput)
    description = forms.CharField(widget=forms.HiddenInput)
    creation_datetime = forms.DateTimeField(widget=forms.HiddenInput)
    expiration_datetime = forms.DateTimeField(widget=forms.HiddenInput)
    deposit_account = forms.CharField(widget=forms.HiddenInput)
    return_url = forms.URLField(widget=forms.HiddenInput)
    notification_url = forms.URLField(widget=forms.HiddenInput)
    signature = forms.CharField(widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        BasePaymentForm.__init__(self, *args, **kwargs)

        assert 'deposit_account' in self.metadata, u"You should specify deposit account"

        products = self.invoice.get_products()

        data = {
            'provider': settings.DEPOSITS_PROVIDER,
            'invoice_id': self.invoice.get_id(),
            'payment_amount': sum(map(lambda x: x.get_price_amount(), products)),
            'payment_currency': self.invoice.get_products()[0].get_price_currency(),
            'description': u", ".join(map(lambda x: unicode(x.get_title()), products)),
            'creation_datetime': self.invoice.get_creation_datetime(),
            'expiration_datetime': self.invoice.get_expiration_datetime(),
            'deposit_account': self.metadata['deposit_account'],
            'return_url': self.return_url,
            'notification_url': settings.DEPOSITS_NOTIFICATION_URL
        }

        data['signature'] = calculate_signature(data)

        forms.Form.__init__(self, initial=data)

    def render(self):
        return self.as_p()
