from datetime import datetime
from django.conf import settings
from django.core.exceptions import PermissionDenied
import requests
from requests.exceptions import RequestException
from cashbox.backends.base import BasePaymentBackend
from cashbox.backends.deposits.forms import DepositsPaymentForm
from cashbox.backends.deposits.utils import calculate_signature
from cashbox.exceptions import InvalidResponse
from cashbox.models import Payment
from cashbox.responses import NotificationResponse


__all__ = ['DepositsPaymentBackend']


class DepositsPaymentBackend(BasePaymentBackend):
    """
    Custom deposits module
    """
    CODE = 'deposits'
    PAYMENT_STATUSES = {
        'registered': Payment.STATUS_PENDING,
        'completed': Payment.STATUS_AUTHORIZED,
        'refunded': Payment.STATUS_REFUND
    }

    def get_form(self, invoice, return_url, metadata=None):
        return DepositsPaymentForm(invoice, return_url, metadata)

    def process_notification(self, notification_request):
        method = notification_request.backend_method

        if method == 'ipn':
            return self._process_ipn_notification(notification_request)

        raise Exception('Unknown payment backend notification')

    def refund(self, payment):
        request = {
            'provider': settings.DEPOSITS_PROVIDER,
            'invoice_id': payment.invoice_id
        }

        request['signature'] = calculate_signature(request)

        response = requests.post(settings.DEPOSITS_REFUND_URL, data=request)

        if response.status_code != 200 or response.text != 'REFUNDED':
            raise InvalidResponse(
                'Can\'t find REFUNDED in response or status wan\'t 200',
                request=unicode(request),
                response=response.text
            )

    def finish(self, payment):
        """
        This backend doesn't support two-phase payments (with hold state)
        """
        pass

    def _process_ipn_notification(self, notification_request):
        data = notification_request.post_args

        received_signature = data.pop('signature')
        calculated_signature = calculate_signature(data)

        if received_signature != calculated_signature:
            raise PermissionDenied("Invalid signature")

        # Parse invoice
        invoice_id = data['invoice_id']

        try:
            payment = Payment.objects.get(invoice_id=invoice_id)
        except Payment.DoesNotExist:
            payment = Payment(invoice_id=invoice_id)

        payment.status = self.PAYMENT_STATUSES[data['status']]
        payment.total_price_amount = float(data['amount'])
        payment.total_price_currency = data['currency']
        if payment.metadata is None:
            payment.metadata = {}
        payment.metadata['internal_id'] = data['internal_id']

        # Generate response
        data['signature'] = received_signature
        data['response_datetime'] = datetime.now().isoformat()
        response_signature = calculate_signature(data)

        return NotificationResponse(
            payment=payment,
            response=u"<response>%s|%s</response>" % (response_signature, data['response_datetime'])
        )

    def is_account_exists(self, deposit_account_email):
        query = {
            'provider': settings.DEPOSITS_PROVIDER,
            'deposit_account': deposit_account_email
        }
        query['signature'] = calculate_signature(query)

        try:
            response = requests.post(settings.DEPOSITS_ACCOUNT_CHECK_URL, query)
            return response.text == 'DEFINED'
        except RequestException:
            return False
