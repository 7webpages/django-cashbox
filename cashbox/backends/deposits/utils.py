import hmac
from django.conf import settings


__all__ = ['calculate_signature']


def calculate_signature(data):
    """
    Generates signature for posted data
    """
    values = map(lambda x: unicode(x[1]), sorted(data.items(), key=lambda x: x[0]))
    message = u''.join(values)
    return hmac.new(settings.DEPOSITS_SECRET, message.encode('utf-8')).hexdigest()
