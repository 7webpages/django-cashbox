# coding=utf-8
from datetime import datetime
from django.conf import settings
from mock import Mock, patch
import os
from django.test import TestCase
from django.test.utils import override_settings
from django.utils.datastructures import SortedDict
from cashbox.backends.payu.backend import PayUPaymentBackend
from cashbox.exceptions import InvalidResponse
from cashbox.models import BaseInvoice, BaseProduct, Payment
from cashbox.requests import NotificationRequest
from cashbox.responses import NotificationResponse


__all__ = ['PayUPaymentBackendTest']


@override_settings(
    PAYU_LU_URL='https://secure.payu.ua/order/lu.php',
    PAYU_IRN_URL='https://secure.payu.ua/order/irn.php',
    PAYU_IDN_URL='https://secure.payu.ua/order/idn.php',
    PAYU_MERCHANT_ID='payu',
    PAYU_SECRET_KEY='AABBCCDDEEFF',
    PAYU_DEBUG=True
)
class PayUPaymentBackendTest(TestCase):
    HTML_DIRECTORY = os.path.join(os.path.dirname(__file__), 'html_examples')
    backend = PayUPaymentBackend()
    maxDiff = None

    @patch('cashbox.backends.payu.backend.datetime')
    def test_process_ipn_notification(self, datetime_mock):
        """
        1. Define IPN notification request
        2. Call process_notification method
        3. Check response
        """
        datetime_mock.now = Mock(return_value=datetime(2012, 4, 26, 12, 34, 34))

        # Step 1
        notification_request = NotificationRequest(
            backend_code='payu',
            backend_method='ipn',
            request_method='POST',
            get_args={},
            post_args=SortedDict([
                ('REFNOEXT', '2985eb300b994ea9902b8d3c68a45529'),
                ('REFNO', '8294084'),
                ('ORDERSTATUS', 'COMPLETE'),
                ('IPN_PAID_AMOUNT', '123.20'),
                ('CURRENCY', 'UAH'),
                ('IPN_PID[]', '1'),
                ('IPN_PNAME[]', 'Apple MacBook Air 13 inch'),
                ('IPN_DATE', '20120426123434'),
                ('HASH', '0ac24e78db15794aa05af5003e5a76da')
            ])
        )

        # Step 2
        notification_response = self.backend.process_notification(notification_request)

        # Step 3
        response, payment = notification_response.response, notification_response.payment

        self.assertIsInstance(notification_response, NotificationResponse)
        self.assertEqual('<EPAYMENT>20120426123434|65402538b73d228a393cdcf9af295f89</EPAYMENT>', response)
        self.assertEqual(Payment.STATUS_COMPLETE, payment.status)
        self.assertEqual('2985eb300b994ea9902b8d3c68a45529', payment.invoice_id)
        self.assertAlmostEqual(123.20, payment.total_price_amount)
        self.assertEqual('UAH', payment.total_price_currency)
        self.assertEqual('8294084', payment.metadata.get('REFNO'))

    @patch('cashbox.backends.payu.backend.datetime')
    @patch('cashbox.backends.payu.backend.requests')
    def test_refund(self, requests_mock, datetime_mock):
        """
        1. Define fake payment object
        2. Mock requests.post with successful response
        3. Mock requests.post with successful response, but with unconfirmed status
        4. Mock requests.post with failed response
        """
        datetime_mock.now = Mock(return_value=datetime(2013, 12, 25, 19, 38, 20))

        # Step 1
        payment = Payment(
            total_price_amount=120.25,
            total_price_currency='UAH',
            metadata={
                'REFNO': '833943'
            }
        )

        # Step 2
        requests_mock.post = Mock(return_value=Mock(**{
            'text': u'<EPAYMENT>8294084|OK|Confirmed.|2013-12-25 19:26:56|fdeb1cf0afe763040d72ef2afbf49278</EPAYMENT>'
        }))
        self.backend.refund(payment)

        requests_mock.post.assert_called_with(settings.PAYU_IRN_URL, data=[
            ('MERCHANT', 'payu'),
            ('ORDER_REF', '833943'),
            ('ORDER_AMOUNT', '120.25'),
            ('ORDER_CURRENCY', 'UAH'),
            ('IRN_DATE', '2013-12-25 19:38:20'),
            ('ORDER_HASH', '08a5af9e16ec0c679c4da1c7a55a6bb5')
        ])

        # Step 2
        requests_mock.post = Mock(return_value=Mock(**{
            'text': u'<EPAYMENT>833943|2|Error|2013-12-25 19:38:23|6cabd88e53b668b1b8b6dd8248456794</EPAYMENT>'
        }))
        self.assertRaises(InvalidResponse, self.backend.refund, payment)

        # Step 3
        requests_mock.post = Mock(return_value=Mock(**{
            'text': u'Some error'
        }))
        self.assertRaises(InvalidResponse, self.backend.refund, payment)

    @patch('cashbox.backends.payu.backend.datetime')
    @patch('cashbox.backends.payu.backend.requests')
    def test_finish(self, requests_mock, datetime_mock):
        """
        1. Define fake payment object
        2. Mock requests.post with successful response
        3. Mock requests.post with successful response, but with unconfirmed status
        4. Mock requests.post with failed response
        """
        datetime_mock.now = Mock(return_value=datetime(2013, 12, 25, 19, 38, 20))

        # Step 1
        payment = Payment(
            total_price_amount=120.25,
            total_price_currency='UAH',
            metadata={
                'REFNO': '833943'
            }
        )

        # Step 2
        requests_mock.post = Mock(return_value=Mock(**{
            'text': u'<EPAYMENT>833943|1|Confirmed|2013-12-25 19:38:23|b55d2d6c90bc4c16f8d7cb07be39546d</EPAYMENT>'
        }))
        self.backend.finish(payment)

        requests_mock.post.assert_called_with(settings.PAYU_IDN_URL, data=[
            ('MERCHANT', 'payu'),
            ('ORDER_REF', '833943'),
            ('ORDER_AMOUNT', '120.25'),
            ('ORDER_CURRENCY', 'UAH'),
            ('IDN_DATE', '2013-12-25 19:38:20'),
            ('ORDER_HASH', '08a5af9e16ec0c679c4da1c7a55a6bb5')
        ])

        # Step 2
        requests_mock.post = Mock(return_value=Mock(**{
            'text': u'<EPAYMENT>833943|2|Error|2013-12-25 19:38:23|6cabd88e53b668b1b8b6dd8248456794</EPAYMENT>'
        }))
        self.assertRaises(InvalidResponse, self.backend.finish, payment)

        # Step 3
        requests_mock.post = Mock(return_value=Mock(**{
            'text': u'Some error'
        }))
        self.assertRaises(InvalidResponse, self.backend.finish, payment)

    @patch('cashbox.backends.payu.forms.datetime')
    def test_get_form(self, datetime_mock):
        """
        1. Create simple invoice with one product
        2. Call method and check results
        3. Call method with metadata and check results
        """
        datetime_mock.now = Mock(return_value=datetime(2013, 12, 21, 12))

        # Step 1
        class Product(BaseProduct):
            def get_title(self):
                return u'Продукт #1'

            def get_code(self):
                return '123'

            def get_price_amount(self):
                return 120.35

            def get_price_currency(self):
                return 'UAH'

        class Invoice(BaseInvoice):
            def get_id(self):
                return 'new-invoice'

            def get_creation_datetime(self):
                return datetime(2013, 12, 21, 10)

            def get_expiration_datetime(self):
                return datetime(2013, 12, 21, 12, 15)

            def get_products(self):
                return [
                    Product()
                ]

        # Step 2
        form = self.backend.get_form(Invoice(), 'http://example.com/')
        sample_html = open(os.path.join(self.HTML_DIRECTORY, 'payment_form.html')).read().decode('utf-8')

        self.assertEqual('POST', form.method)
        #self.assertEqual('https://secure.payu.ua/order/lu.php', form.action)
        self.assertHTMLEqual(sample_html, form.render())

        # Step 3
        form = self.backend.get_form(Invoice(), 'http://example.com/', metadata={
            'ORDER_PINFO[]': ['TEST']
        })
        sample_html = open(
            os.path.join(self.HTML_DIRECTORY, 'payment_form_with_product_info.html')
        ).read().decode('utf-8')

        self.assertHTMLEqual(sample_html, form.render())
