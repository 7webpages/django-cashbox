from collections import Iterable
from copy import deepcopy
from datetime import datetime
from xml.sax.saxutils import quoteattr
from django.conf import settings
from django.utils.datastructures import SortedDict
from cashbox.backends.payu.utils import generate_signature
from cashbox.forms import BasePaymentForm


__all__ = ['PayUPaymentForm']


class PayUPaymentForm(BasePaymentForm):
    method = 'POST'
    action = settings.PAYU_LU_URL

    def render(self):
        fields_data = self._get_fields_data_from_invoice(self.invoice)
        render_field = lambda f, v: u'<input type="hidden" name=%s value=%s />\n' % (quoteattr(f), quoteattr(v))

        html_form = u""
        for field, values in fields_data.iteritems():
            if isinstance(values, basestring) or not isinstance(values, Iterable):
                values = [values]

            for value in values:
                html_form += render_field(field, value)

        return html_form

    def _get_fields_data_from_invoice(self, invoice):
        products = invoice.get_products()

        assert len(products) > 0

        products_codes = map(lambda p: p.get_code(), products)
        products_titles = map(lambda p: p.get_title(), products)
        products_prices = map(lambda p: "%.2f" % p.get_price_amount(), products)

        data = SortedDict([
            ('MERCHANT', settings.PAYU_MERCHANT_ID),
            ('ORDER_REF', invoice.get_id()),
            ('ORDER_DATE', invoice.get_creation_datetime().strftime('%Y-%m-%d %H:%M:%S')),
            ('ORDER_PNAME[]', products_titles),
            ('ORDER_PCODE[]', products_codes),
            ('ORDER_PINFO[]', self.metadata.get('ORDER_PINFO[]', [])),
            ('ORDER_PRICE[]', products_prices),
            ('ORDER_QTY[]', ['1'] * len(products)),
            ('ORDER_VAT[]', ['0'] * len(products)),
            ('ORDER_SHIPPING', '0'),
            ('PRICES_CURRENCY', products[0].get_price_currency()),
            ('ORDER_PRICE_TYPE[]', ['GROSS'] * len(products)),
            ('BACK_REF', self.return_url),
            ('ORDER_TIMEOUT', str((invoice.get_expiration_datetime() - datetime.now()).seconds)),
            ('TIMEOUT_URL', self.return_url)
        ])

        data['ORDER_HASH'] = self._get_order_hash(data)

        return data

    def _get_order_hash(self, data):
        data = deepcopy(data)

        hash_free_fields = ['TESTORDER', 'DEBUG', 'BACK_REF', 'ORDER_TIMEOUT', 'TIMEOUT_URL']

        for field in hash_free_fields:
            if field in data:
                del data[field]

        return generate_signature(data)
