import re
from django.conf import settings
import requests
from copy import deepcopy
from datetime import datetime
from django.core.exceptions import PermissionDenied
from django.utils.datastructures import SortedDict
from cashbox.backends.base import BasePaymentBackend
from cashbox.backends.payu.forms import PayUPaymentForm
from cashbox.backends.payu.utils import generate_signature
from cashbox.exceptions import InvalidResponse
from cashbox.models import Payment
from cashbox.responses import NotificationResponse


__all__ = ['PayUPaymentBackend']


class PayUPaymentBackend(BasePaymentBackend):
    """
    http://www.payu.ua/
    """
    CODE = 'payu'

    ORDER_STATUSES = {
        'ORDER_AUTHORIZED': Payment.STATUS_AUTHORIZED,
        'PAYMENT_AUTHORIZED': Payment.STATUS_AUTHORIZED,
        'PAYMENT_RECEIVED': Payment.STATUS_AUTHORIZED,
        'COMPLETE': Payment.STATUS_COMPLETE,
        'REVERSED': Payment.STATUS_REVERSED,
        'REFUND': Payment.STATUS_REFUND,
        'TEST': Payment.STATUS_AUTHORIZED if settings.PAYU_DEBUG else Payment.STATUS_FAILED
    }

    def get_form(self, invoice, return_url, metadata=None):
        return PayUPaymentForm(invoice, return_url, metadata=metadata)

    def process_notification(self, notification_request):
        method = notification_request.backend_method

        if method == 'ipn':
            return self._process_ipn_notification(notification_request)

        raise Exception('Unknown payment backend notification')

    def refund(self, payment):
        # Prepare IRN request
        irn_request = SortedDict([
            ('MERCHANT', settings.PAYU_MERCHANT_ID),
            ('ORDER_REF', payment.metadata['REFNO']),
            ('ORDER_AMOUNT', u"%.2f" % payment.total_price_amount),
            ('ORDER_CURRENCY', payment.total_price_currency),
            ('IRN_DATE', datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        ])
        irn_request['ORDER_HASH'] = generate_signature(irn_request)

        # Send IRN notification
        irn_response = requests.post(settings.PAYU_IRN_URL, data=irn_request.items())

        # Parse IRN response
        try:
            confirmation = re.findall('<EPAYMENT>(.*)</EPAYMENT>', irn_response.text)[0]
            order_ref, response_code, response_msg, irn_date, order_hash = confirmation.split('|')
        except IndexError:
            raise InvalidResponse(
                'Can\'t find <epayment> element in IRN response',
                request=irn_request,
                response=irn_response.text
            )

        if response_code != 'OK':
            raise InvalidResponse(
                'IRN response code was n\'t OK',
                request=irn_request,
                response=irn_response.text
            )

        # Check response signature
        response = SortedDict([
            ('ORDER_REF', order_ref),
            ('RESPONSE_CODE', response_code),
            ('RESPONSE_MSG', response_msg),
            ('IRN_DATE', irn_date)
        ])
        control_hash = generate_signature(response)

        if control_hash != order_hash:
            raise PermissionDenied("Invalid signature")

    def finish(self, payment):
        # Prepare IDN request
        idn_request = SortedDict([
            ('MERCHANT', settings.PAYU_MERCHANT_ID),
            ('ORDER_REF', payment.metadata['REFNO']),
            ('ORDER_AMOUNT', "%.2f" % payment.total_price_amount),
            ('ORDER_CURRENCY', payment.total_price_currency),
            ('IDN_DATE', datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
        ])
        idn_request['ORDER_HASH'] = generate_signature(idn_request)

        # Send IDN notification
        idn_response = requests.post(settings.PAYU_IDN_URL, data=idn_request.items())

        # Parse IDN response
        try:
            confirmation = re.findall('<EPAYMENT>(.*)</EPAYMENT>', idn_response.text)[0]
            order_ref, response_code, response_msg, idn_date, order_hash = confirmation.split('|')
        except IndexError:
            raise InvalidResponse(
                'Can\'t find <epayment> element in IDN response',
                request=idn_request,
                response=idn_response.text
            )

        if response_code != '1':
            raise InvalidResponse(
                'IDN response code was n\'t 1 (Confirm)',
                request=idn_request,
                response=idn_response.text
            )

        # Check response signature
        response = SortedDict([
            ('ORDER_REF', order_ref),
            ('RESPONSE_CODE', response_code),
            ('RESPONSE_MSG', response_msg),
            ('IDN_DATE', idn_date)
        ])
        control_hash = generate_signature(response)

        if control_hash != order_hash:
            raise PermissionDenied("Invalid signature")

    def _process_ipn_notification(self, notification_request):
        data = notification_request.post_args

        # Check request signature
        data_for_signature = deepcopy(data)
        del data_for_signature['HASH']
        control_hash = generate_signature(data_for_signature)

        if data['HASH'] != control_hash:
            raise PermissionDenied("Invalid signature")

        # Get payment object
        invoice_id = data['REFNOEXT']

        if data['ORDERSTATUS'] not in self.ORDER_STATUSES:
            raise PermissionDenied("Invalid order status")

        try:
            payment = Payment.objects.get(invoice_id=invoice_id)
        except Payment.DoesNotExist:
            payment = Payment(invoice_id=invoice_id)

        payment.status = self.ORDER_STATUSES[data['ORDERSTATUS']]
        payment.total_price_amount = float(data.get('IPN_PAID_AMOUNT', data.get('IPN_TOTALGENERAL')))
        payment.total_price_currency = 'UAH'
        payment.metadata = {
            'REFNO': data['REFNO']
        }

        # Get response signature
        ipn_pid = data['IPN_PID[]']
        ipn_pname = data['IPN_PNAME[]']
        ipn_date = data['IPN_DATE']
        response_date = datetime.now().strftime('%Y%m%d%H%M%S')

        response_hash = generate_signature(
            SortedDict([
                ('IPN_PID[]', ipn_pid[0] if isinstance(ipn_pid, list) else ipn_pid),
                ('IPN_PNAME[]', ipn_pname[0] if isinstance(ipn_pname, list) else ipn_pname),
                ('IPN_DATE', ipn_date),
                ('DATE', response_date)
            ])
        )

        response = u"<EPAYMENT>%s|%s</EPAYMENT>" % (response_date, response_hash)

        return NotificationResponse(payment=payment, response=response)
