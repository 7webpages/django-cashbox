from collections import Iterable
import hashlib
import hmac
from django.conf import settings


def generate_signature(ordered_data_dict):
    """
    Generates signature according to the payu algorithm
    """
    base_string = u""

    for field, values in ordered_data_dict.iteritems():
        if isinstance(values, basestring) or not isinstance(values, Iterable):
            values = [values]

        for value in values:
            if value is None:
                value = u""
            base_string += u"%d%s" % (len(value.encode('utf-8')), value)

    return hmac.new(
        settings.PAYU_SECRET_KEY.encode('utf-8'),
        base_string.encode('utf-8'),
        hashlib.md5
    ).hexdigest()
