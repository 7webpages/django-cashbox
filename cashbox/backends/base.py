class BasePaymentBackend(object):
    """
    Base class for all payment backends
    """
    CODE = 'unset'  # UID for payment backend

    def process_notification(self, notification_request):
        """
        Processes all http requests from payment service

        Args:
            notification_request (requests.NotificationRequest): Wrapped http request from payment service

        Returns:
            BaseNotificationResponse: Wrapped http response for payment service
        """
        raise NotImplementedError("Redefine this in derived classes")

    def refund(self, payment):
        """
        Cancels payment

        Args:
            payment (models.Payment)
        """
        raise NotImplementedError("Redefine this in derived classes")

    def finish(self, payment):
        """
        Confirms payment (for example delivery confirmation for double-phase payments)

        Args:
            payment (models.Payment)
        """
        raise NotImplementedError("Redefine this in derived classes")

    def get_form(self, invoice, return_url, metadata=None):
        """
        Generates form which can be used for starting payment

        Args:
            invoice (models.BaseInvoice)
            return_url (str): User will be redirected to this url by payment system (in any case: error or success)
            metadata (dict): Backend-specific attributes
        """
        raise NotImplementedError("Redefine this in derived classes")
