class UnsupportedBackend(Exception):
    pass


class InvalidResponse(Exception):
    def __init__(self, message, request=None, response=None, original_traceback=None):
        self.request = request if not isinstance(request, unicode) else request.encode('utf8')
        self.response = response if not isinstance(response, unicode) else response.encode('utf8')
        self.original_traceback = original_traceback

        super(InvalidResponse, self).__init__(message)

    def __str__(self):
        text = str(self.message)

        if self.request:
            text += "\n\n\nRequest was: \n" + self.request
        if self.response:
            text += "\n\n\nResponse was: \n" + self.response
        if self.original_traceback:
            text += "\n\n\nOriginal traceback: \n" + str(self.original_traceback)

        return text
