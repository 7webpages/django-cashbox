class NotificationRequest(object):
    """
    Wrapped http request from external payment system
    """
    def __init__(self, backend_code, backend_method, request_method, get_args, post_args, meta=None):
        """
        All arguments should be serializable

        Args:
            backend_code (str): PaymentBackend.code
            backend_method (str): For example ipn
            request_method (str): POST or GET
            get_args (dict): request.GET
            post_args (dict): request.POST
            meta (dict): Additional information about request, for example user_ip
        """
        self.backend_code = backend_code
        self.backend_method = backend_method
        self.request_method = request_method
        self.get_args = get_args
        self.post_args = post_args
        self.meta = meta

    def to_dict(self):
        return self.__dict__