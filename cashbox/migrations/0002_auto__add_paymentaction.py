# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PaymentAction'
        db.create_table(u'cashbox_paymentaction', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('payment', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='actions', null=True, to=orm['cashbox.Payment'])),
            ('payment_state', self.gf('json_field.fields.JSONField')(default=u'null')),
            ('backend', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('request', self.gf('json_field.fields.JSONField')(default=u'null')),
            ('response', self.gf('json_field.fields.JSONField')(default=u'null')),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('internal_exception', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('internal_exception_traceback', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('cashbox', ['PaymentAction'])


    def backwards(self, orm):
        # Deleting model 'PaymentAction'
        db.delete_table(u'cashbox_paymentaction')


    models = {
        'cashbox.payment': {
            'Meta': {'object_name': 'Payment'},
            'backend': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'metadata': ('json_field.fields.JSONField', [], {'default': "u'null'"}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'total_price_amount': ('django.db.models.fields.FloatField', [], {}),
            'total_price_currency': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'cashbox.paymentaction': {
            'Meta': {'object_name': 'PaymentAction'},
            'backend': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_exception': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'internal_exception_traceback': ('django.db.models.fields.TextField', [], {}),
            'payment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'actions'", 'null': 'True', 'to': "orm['cashbox.Payment']"}),
            'payment_state': ('json_field.fields.JSONField', [], {'default': "u'null'"}),
            'request': ('json_field.fields.JSONField', [], {'default': "u'null'"}),
            'response': ('json_field.fields.JSONField', [], {'default': "u'null'"})
        }
    }

    complete_apps = ['cashbox']