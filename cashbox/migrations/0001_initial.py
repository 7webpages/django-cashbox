# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Payment'
        db.create_table(u'cashbox_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('backend', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('invoice_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
            ('total_price_amount', self.gf('django.db.models.fields.FloatField')()),
            ('total_price_currency', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('last_update', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('metadata', self.gf('json_field.fields.JSONField')(default=u'null')),
        ))
        db.send_create_signal(u'cashbox', ['Payment'])


    def backwards(self, orm):
        # Deleting model 'Payment'
        db.delete_table(u'cashbox_payment')


    models = {
        u'cashbox.payment': {
            'Meta': {'object_name': 'Payment'},
            'backend': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'metadata': ('json_field.fields.JSONField', [], {'default': "u'null'"}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'total_price_amount': ('django.db.models.fields.FloatField', [], {}),
            'total_price_currency': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        }
    }

    complete_apps = ['cashbox']