import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-cashbox',
    version='0.1.6',
    packages=find_packages(),
    include_package_data=True,
    description='Cashbox is a way to integrate different payment system under common internal api',
    long_description=README,
    author='Dmytro Popovych',
    author_email='drudim.ua@gmail.com',
    install_requires=[
        'Django>=1.4',
        'django-json-field>=0.5.5',
        'django-celery>=3.0.21',
        'requests>=1.2.3',
        'mock>=1.0.1'
    ],
    test_suite='run_tests'
)
