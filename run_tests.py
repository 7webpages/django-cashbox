import sys
from django.conf import settings

settings.configure(
    DEBUG=True,
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
        }
    },
    ROOT_URLCONF='cashbox.urls',
    INSTALLED_APPS=(
        'cashbox',
    ),

    # TODO: Rewrite backends importing to avoid such imports
    PAYU_DEBUG=True,
    PAYU_LU_URL='',
    DEPOSITS_REGISTRATION_URL=''
)

from django.test.simple import DjangoTestSuiteRunner

test_runner = DjangoTestSuiteRunner(verbosity=1)

failures = test_runner.run_tests(['cashbox', ])
if failures:
    sys.exit(failures)