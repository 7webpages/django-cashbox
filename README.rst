=======
Cashbox
=======

Cashbox is a way to integrate different payment system under common internal api.

Quick start
-----------

1. Add "cashbox" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'cashbox',
    )

2. Include the cashbox URLconf in your project urls.py like this::

    url(r'^cashbox/', include('cashbox.urls')),

3. Run `python manage.py migrate` to create the cashbox models.
